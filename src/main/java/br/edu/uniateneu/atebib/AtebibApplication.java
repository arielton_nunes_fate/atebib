package br.edu.uniateneu.atebib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan("br.edu.uniateneu.atebib.modelo*")
@SpringBootApplication
public class AtebibApplication {

	public static void main(String[] args) {
		SpringApplication.run(AtebibApplication.class, args);
	}

}
