package br.edu.uniateneu.atebib.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.atebib.modelo.Aluno;
import br.edu.uniateneu.atebib.modelo.Emprestimo;
import br.edu.uniateneu.atebib.modelo.ResponseModel;
import br.edu.uniateneu.atebib.repository.AlunoRepository;


@RestController
@RequestMapping(value = "/alunos")
public class AlunoService {
	@Autowired
	AlunoRepository alunoRepository;

	@Produces("application/json")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Aluno> retorna() {
		return (ArrayList<Aluno>) alunoRepository.findAll();
	}

	@Consumes("application/json")
	@Produces("application/json")
	@RequestMapping(value = "/salvar", method = RequestMethod.POST)
	public @ResponseBody Aluno salvar(@RequestBody Aluno aluno) {
		Aluno user  = this.alunoRepository.save(aluno);
		return user;
	}

	@Consumes("application/json")
	@Produces("application/json")
	@RequestMapping(value = "/salvar", method = RequestMethod.PUT)
	public @ResponseBody Aluno atualizar(@RequestBody Aluno aluno) {
		Aluno user = this.alunoRepository.save(aluno);
		return user;
	}

	@Produces("application/json")
	@RequestMapping(value = "/deletar/{codigo}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {
		try {
		Aluno aluno = alunoRepository.getReferenceById(codigo);
		System.out.println("********"+ aluno.getId());
		
		
		if (alunoRepository.findById(codigo).isEmpty()) {
			return new ResponseModel(500, "Aluno não encontrado.");
		}
		
			alunoRepository.delete(aluno);
			return new ResponseModel(200, "Registro excluido com sucesso!");

		} catch (Exception e) {
			return new ResponseModel(500, e.getMessage());
		}

	}
	
	@Produces("application/json")
	@RequestMapping(value = "/{codigo}/emprestimos/list", method = RequestMethod.GET)
	public @ResponseBody List<Emprestimo> emprestimos(@PathVariable("codigo") Long codigo) {
		
		Aluno aluno = alunoRepository.getReferenceById(codigo);
		return aluno.getEmprestimos();

	}
}
