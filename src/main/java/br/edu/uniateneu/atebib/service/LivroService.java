package br.edu.uniateneu.atebib.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.atebib.modelo.ResponseModel;
import br.edu.uniateneu.atebib.modelo.Livro;
import br.edu.uniateneu.atebib.repository.LivroRepository;


@RestController
@RequestMapping(value = "/livros")
public class LivroService {
	@Autowired
	LivroRepository livroRepository;

	@Produces("application/json")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Livro> retorna() {
		return (ArrayList<Livro>) livroRepository.findAll();
	}

	@Consumes("application/json")
	@Produces("application/json")
	@RequestMapping(value = "/salvar", method = RequestMethod.POST)
	public @ResponseBody Livro salvar(@RequestBody Livro livro) {
		Livro user  = this.livroRepository.save(livro);
		return user;
	}

	@Consumes("application/json")
	@Produces("application/json")
	@RequestMapping(value = "/salvar", method = RequestMethod.PUT)
	public @ResponseBody Livro atualizar(@RequestBody Livro livro) {
		Livro user = this.livroRepository.save(livro);
		return user;
	}

	@Produces("application/json")
	@RequestMapping(value = "/deletar/{codigo}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {
		try {
		Livro livro = livroRepository.getReferenceById(codigo);
		System.out.println("********"+ livro.getId());

		if (livro.getId().equals(null)) {
			return new ResponseModel(500, "Livro não encontrado.");
		}
		
			livroRepository.delete(livro);
			return new ResponseModel(200, "Registro excluido com sucesso!");

		} catch (Exception e) {
			return new ResponseModel(500, e.getMessage());
		}

	}
}
