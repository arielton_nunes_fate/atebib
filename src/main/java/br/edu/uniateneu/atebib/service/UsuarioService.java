package br.edu.uniateneu.atebib.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.atebib.modelo.ResponseModel;
import br.edu.uniateneu.atebib.modelo.Usuario;
import br.edu.uniateneu.atebib.repository.UsuarioRepository;


@RestController
@RequestMapping(value = "/usuarios")
public class UsuarioService {
	@Autowired
	UsuarioRepository usuarioRepository;

	@Produces("application/json")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Usuario> retorna() {
		return (ArrayList<Usuario>) usuarioRepository.findAll();
	}

	@Consumes("application/json")
	@Produces("application/json")
	@RequestMapping(value = "/salvar", method = RequestMethod.POST)
	public @ResponseBody Usuario salvar(@RequestBody Usuario usuario) {
		Usuario user  = this.usuarioRepository.save(usuario);
		return user;
	}

	@Consumes("application/json")
	@Produces("application/json")
	@RequestMapping(value = "/salvar", method = RequestMethod.PUT)
	public @ResponseBody Usuario atualizar(@RequestBody Usuario usuario) {
		Usuario user = this.usuarioRepository.save(usuario);
		return user;
	}

	@Produces("application/json")
	@RequestMapping(value = "/deletar/{codigo}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {
		try {
		Usuario usuario = usuarioRepository.getReferenceById(codigo);
		System.out.println("********"+ usuario.getId());
		
		
		if (usuarioRepository.findById(codigo).isEmpty()) {
			return new ResponseModel(500, "Usuário não encontrado.");
		}
		
			usuarioRepository.delete(usuario);
			return new ResponseModel(200, "Registro excluido com sucesso!");

		} catch (Exception e) {
			return new ResponseModel(500, e.getMessage());
		}

	}
}
