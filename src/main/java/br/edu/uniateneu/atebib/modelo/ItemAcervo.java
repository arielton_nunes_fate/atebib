package br.edu.uniateneu.atebib.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name="tb_item_acervo")
public class ItemAcervo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="cd_item_acervo")
	private Long id;
	@ManyToOne
	private Livro livro;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Livro getLivro() {
		return livro;
	}
	public void setLivro(Livro livro) {
		this.livro = livro;
	}

}
