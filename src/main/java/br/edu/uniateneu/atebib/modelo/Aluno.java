package br.edu.uniateneu.atebib.modelo;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tb_aluno")
public class Aluno {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="cd_aluno")
	private Long id;
	@Column(name="ds_nome")
	private String nome;
	@Column(name="ds_matricula")
	private String matricula;
	@Column(name="ds_cpf")
	private String cpf;
	@Column(name="ds_rg")
	private String rg;
	@Column(name="dt_nascimento")
	private Date dataNascimento;
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="cd_usuario")
	private Usuario usuario;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="tb_curso_aluno",
	joinColumns={@JoinColumn(name="cd_curso")},
	inverseJoinColumns={@JoinColumn(name="cd_aluno")})
	private List<Curso> cursos;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="tb_aluno_emprestimo",
	joinColumns={@JoinColumn(name="cd_emprestimo")},
	inverseJoinColumns={@JoinColumn(name="cd_aluno")})
	private List<Emprestimo> emprestimos;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public List<Curso> getCursos() {
		return cursos;
	}
	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
	public List<Emprestimo> getEmprestimos() {
		return emprestimos;
	}
	public void setEmprestimos(List<Emprestimo> emprestimos) {
		this.emprestimos = emprestimos;
	}


}
