package br.edu.uniateneu.atebib.modelo;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_multa")
public class Multa {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="cd_multa")
	private Long id;
	@Column(name ="vl_diario")
	private Double vlrDiario;
	@Column(name ="nr_prazo" )
	private Integer prazo;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Double getVlrDiario() {
		return vlrDiario;
	}
	public void setVlrDiario(Double vlrDiario) {
		this.vlrDiario = vlrDiario;
	}
	public Integer getPrazo() {
		return prazo;
	}
	public void setPrazo(Integer prazo) {
		this.prazo = prazo;
	}
}
