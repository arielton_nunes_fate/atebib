package br.edu.uniateneu.atebib.modelo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@Table(name="tb_funcionario")
public class Funcionario {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="cd_funcionario")
	private Long id;
	@Column(name="ds_nome")
	private String nome;
	@Column(name="ds_matricula")
	private String matricula;
	@Column(name="ds_cpf")
	private String cpf;
	@Column(name="ds_rg")
	private String rg;
	@Column(name="dt_nascimento")
	private Date dataNascimento;
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="cd_usuario")
	private Usuario usuario;
	@Column(name="ds_funcao")
	private String funcao;
	
	
	
	public String getFuncao() {
		return funcao;
	}
	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
}
