package br.edu.uniateneu.atebib.modelo;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="tb_emprestimo")
public class Emprestimo {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name="cd_emprestimo")
private Long id;
@OneToOne
private ItemAcervo livro;
@OneToOne
private Usuario usuario;
@Temporal(TemporalType.DATE)
@Column(name="dt_emprestimo")
private Date dtEmprestimo;
@Temporal(TemporalType.DATE)
@Column(name="dt_devolucao")
private Date dtDevolucao;
@Column(name="vr_multa")
private Double vrMulta;
@ManyToMany(mappedBy = "emprestimos", cascade = CascadeType.ALL)
private List<Aluno> alunos;
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public ItemAcervo getLivro() {
	return livro;
}
public void setLivro(ItemAcervo livro) {
	this.livro = livro;
}
public Usuario getUsuario() {
	return usuario;
}
public void setUsuario(Usuario usuario) {
	this.usuario = usuario;
}
public Date getDtEmprestimo() {
	return dtEmprestimo;
}
public void setDtEmprestimo(Date dtEmprestimo) {
	this.dtEmprestimo = dtEmprestimo;
}
public Date getDtDevolucao() {
	return dtDevolucao;
}
public void setDtDevolucao(Date dtDevolucao) {
	this.dtDevolucao = dtDevolucao;
}
public Double getVrMulta() {
	return vrMulta;
}
public void setVrMulta(Double vrMulta) {
	this.vrMulta = vrMulta;
}
public List<Aluno> getAlunos() {
	return alunos;
}
public void setAlunos(List<Aluno> alunos) {
	this.alunos = alunos;
}



}
