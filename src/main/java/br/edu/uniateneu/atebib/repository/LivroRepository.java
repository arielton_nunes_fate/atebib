package br.edu.uniateneu.atebib.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.uniateneu.atebib.modelo.Livro;
@Repository
public interface LivroRepository extends JpaRepository<Livro, Long> {

}
