package br.edu.uniateneu.atebib.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.edu.uniateneu.atebib.modelo.Emprestimo;

public interface EmprestimoRepository extends JpaRepository<Emprestimo,Long>{
	 @Query(value="select * from tb_emprestimo "
		 		+ "where dt_devolucao is null", nativeQuery=true)
	List<Emprestimo> getEmprestimosList();
	
}
